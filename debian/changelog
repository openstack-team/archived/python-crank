python-crank (0.7.2-6) unstable; urgency=medium

  * Standard-Version: 4.6.1.
  * Add py3.11-getargspec-is-removed.patch (Closes: #1028731).

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Jan 2023 09:10:04 +0100

python-crank (0.7.2-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add trailing tilde to min version depend to allow
    backports.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * Removed Python 2 support (Closes: #937668).
  * d/copyright: fixed ordering and years.

 -- Thomas Goirand <zigo@debian.org>  Sun, 06 Oct 2019 12:11:53 +0200

python-crank (0.7.2-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.

  [ Thomas Goirand ]
  * Standards-Version: 4.3.0 (no change).
  * Add remove-borken-test-1.patch (Closes: #917506).
  * Using pkgos-dh_auto_install.
  * Using debhelper 10.

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Jan 2019 17:10:10 +0100

python-crank (0.7.2-3) unstable; urgency=medium

  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.

 -- Ondřej Nový <novy@ondrej.org>  Sun, 28 Feb 2016 15:34:44 +0100

python-crank (0.7.2-2) unstable; urgency=medium

  * Fixed FTBFS by adding python-webob as build-depends (Closes: #802128).
  * Fixed watch file to use Github tag rather than broken PyPi.
  * Doing the unit tests with nose as test runner.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2015 14:01:10 +0000

python-crank (0.7.2-1) unstable; urgency=medium

  * Initial release. (Closes: #792249)

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 Jul 2015 10:40:09 +0200
